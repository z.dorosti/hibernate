package caching;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class HibernateMain {
    static SessionFactory sessionFactory = new Configuration().configure("./caching/hibernate.cfg.xml").buildSessionFactory();

    public static void main(String[] args) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("from Person where id=23");
        query.setCacheable(true);
        Person person = (Person) query.uniqueResult();

        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        session.beginTransaction();

        Query query1 = session.createQuery("from Person where id=23");
        query1.setCacheable(true);
        Person person1 = (Person) query1.uniqueResult();

        session.getTransaction().commit();
        session.close();

        System.out.println(person.toString());
        System.out.println(person1.toString());

        session = sessionFactory.openSession();
        session.beginTransaction();

        Query query3 = session.createQuery("from Person where id=23");
        query3.setCacheable(true);
        Person person3 = (Person) query3.uniqueResult();

        session.getTransaction().commit();
        session.close();
        System.out.println(person3.toString());
    }
}
