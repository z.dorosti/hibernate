import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;

import java.util.List;

public class HibernateMain {
    static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public static void main(String[] args) {

        Session session = sessionFactory.getCurrentSession();
        Transaction txn = session.beginTransaction();

        /*
        * SELECT *
        * FROM tbl_person p
        * JOIN tbl_address a ON p.col_home_fk=a.id
        * WHERE col_province='Tehran'
        * ORDER BY p.col_name, p.col_family
        */

        //query by example : selfStudy
        Criteria criteria = session.createCriteria(Person.class, "p");
        criteria.createAlias("p.home", "addr");
        criteria.add(Restrictions.eq("addr.province", "Tehran"));
        criteria.addOrder(Order.asc("p.name")).addOrder(Order.asc("p.family"));

        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.property("p.name"), "name");
        projectionList.add(Projections.property("p.family"), "family");
        projectionList.add(Projections.property("addr.city"), "city");
        projectionList.add(Projections.property("addr.street"), "street");

        criteria.setProjection(projectionList);

        criteria.setResultTransformer(Transformers.aliasToBean(PersonDTO.class));

        List<PersonDTO> list = criteria.list();

        txn.commit();
        session.close();

        for(PersonDTO p : list){
            System.out.println(p.toString());
        }

    }
}
